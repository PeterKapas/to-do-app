import Form from './components/Form'
import Header from './components/Header'
import TodoList from './components/TodoList'
import { useState, useEffect } from 'react'

export default function App() {
	//useState
	const [inputText, setInputText] = useState('')
	const [todos, setTodos] = useState([])
	const [status, setStatus] = useState('all')
	const [filteredTodos, setFilteredTodos] = useState([])

	//useEffect
	useEffect(() => {
		getLocalTodos()
	}, [])

	useEffect(() => {
		filterHandler()
		saveLocalTodos()
	}, [todos, status])
	//functions
	const filterHandler = () => {
		switch (status) {
			case 'completed':
				setFilteredTodos(todos.filter((todo) => todo.completed === true))
				break
			case 'incompleted':
				setFilteredTodos(todos.filter((todo) => todo.completed === false))
				break
			default:
				setFilteredTodos(todos)
				break
		}
	}

	//save to local storage
	const saveLocalTodos = () => {
		localStorage.setItem('todos', JSON.stringify(todos))
	}

	const getLocalTodos = () => {
		if (localStorage.getItem('todos') === null) {
			localStorage.setItem('todos', JSON.stringify([]))
		} else {
			let todoLocal = JSON.parse(localStorage.getItem('todos'))
			setTodos(todoLocal)
		}
	}

	return (
		<div className='App'>
			<Header todos={todos} />
			<Form
				todos={todos}
				setTodos={setTodos}
				setInputText={setInputText}
				inputText={inputText}
				setStatus={setStatus}
			/>
			<TodoList
				todos={todos}
				setTodos={setTodos}
				filteredTodos={filteredTodos}
			/>
		</div>
	)
}
