import React from 'react'

const Header = ({ todos }) => {
	const todosCount = todos.length
	const countIncompleteTodos = () => {
		let countIncompleted = []
		todos.forEach((todo) => {
			if (!todo.completed) {
				countIncompleted.push('incomplete text')
			}
		})
		return countIncompleted.length
	}

	return (
		<header>
			<h1>Peter's Todo List</h1>
			<h4>All todos: {todosCount}</h4>
			<h5>Incompleted todos: {countIncompleteTodos()}</h5>
			<h5>Completed todos: {todosCount - countIncompleteTodos()}</h5>
		</header>
	)
}

export default Header
