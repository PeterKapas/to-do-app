const Form = ({ todos, setTodos, inputText, setInputText, setStatus }) => {
	const inputTextHandler = (e) => {
		setInputText(e.target.value)
	}
	const submitTodoHandler = (e) => {
		e.preventDefault()
		setTodos([
			...todos,
			{ text: inputText, completed: false, id: Math.random() * 10000 },
		])
		setInputText('')
	}
	const statusHandler = (e) => {
		setStatus(e.target.value)
	}
	return (
		<form>
			<input
				value={inputText}
				onChange={inputTextHandler}
				type='text'
				className='todo-input'
				placeholder='Add your todo here...'
			/>
			<button onClick={submitTodoHandler} className='todo-button' type='submit'>
				<i className='fas fa-plus-square'></i>
			</button>
			<div className='select'>
				<select onChange={statusHandler} name='todos' className='filter-todo'>
					<option value='all'>All todos</option>
					<option value='completed'>Completed</option>
					<option value='incompleted'>Incompleted</option>
				</select>
			</div>
		</form>
	)
}

export default Form
